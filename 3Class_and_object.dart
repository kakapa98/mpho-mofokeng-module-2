class MTNAppWinners {
  List<String> catagory = [
    "Public sector",
    "Private sector",
    "Private sector",
    "Public sector",
    "Public sector",
    "Public sector",
    "Private sector",
    "Private sector",
    "Public sector",
    "Private sector",
  ];

  List<String> developers = [
    "Fredrick Kobo",
    "Kobus Ehhlers",
    "Jasper Van Heesch",
    "Simon Hartley",
    "Thato Marumo",
    "Arno Van Helden",
    "Mathew Piper",
    "Alex Thompson",
    "Charles Savage",
    "Mukundi Lambani"
  ];

  List<int> years = [
    2012,
    2013,
    2014,
    2015,
    2016,
    2017,
    2018,
    2019,
    2020,
    2021
  ];

  List upperNames(List apps) {
    List<String> uppercaseNames = [];
    for (var i = 0; i < apps.length; i++) {
      uppercaseNames.add(apps[i].toString().toUpperCase());
    }
    return uppercaseNames;
  }
}

void main() {
  List<String> apps = [
    'FNB-Banking App',
    'SnapScan',
    'Live inspect',
    'WumDrop',
    'Domestly',
    'Standard Bank Shyft',
    'Khula',
    'Naked Insurance',
    'Easy Equities',
    'Ambani'
  ];

  MTNAppWinners winnersApps = new MTNAppWinners();

  List<dynamic> appsNames = winnersApps.upperNames(apps);
  List<dynamic> devs = winnersApps.developers;
  List<dynamic> sectors = winnersApps.catagory;
  List<dynamic> years = winnersApps.years;

  print("MTN Business App of the Year:");
  for (var k = 0; k < winnersApps.upperNames(apps).length; k++) {
    var app = appsNames[k];
    var dev = devs[k];
    var sector = sectors[k];
    var year = years[k];

    print("{AppName: $app | Sector: $sector | Developer: $dev | Year: $year}");
  }
}
